use crate::Solver;

pub struct DayTenSolver {
    lines: Vec<String>,
}

fn get_corrupt_line_val(line: &str) -> usize {
    let mut stack: Vec<char> = vec![];
    for ch in line.chars() {
        match ch {
            // openings
            '[' => stack.push(']'),
            '<' => stack.push('>'),
            '{' => stack.push('}'),
            '(' => stack.push(')'),
            _ => {
                // closings: if it matches the last character, then pop it, otherwise return its value
                if stack[stack.len() - 1] == ch {
                    stack.pop();
                } else {
                    return match ch {
                        ')' => 3,
                        ']' => 57,
                        '}' => 1197,
                        '>' => 25137,
                        _ => panic!("Unexpected character! {}", ch),
                    };
                }
            }
        }
    }
    0
}

fn get_missing_token_val(line: &str) -> usize {
    let mut stack: Vec<char> = vec![];
    for ch in line.chars() {
        match ch {
            // openings
            '[' => stack.push(']'),
            '<' => stack.push('>'),
            '{' => stack.push('}'),
            '(' => stack.push(')'),
            _ => {
                // closings: if it matches the last character, then pop it, otherwise return its value
                if stack[stack.len() - 1] == ch {
                    stack.pop();
                } else {
                    // corrupt line, no value
                    return 0;
                }
            }
        }
    }
    // we should be left with the remaining end brackets
    let mut sum = 0;
    while let Some(ch) = stack.pop() {
        sum *= 5;
        sum += match ch {
            ')' => 1,
            ']' => 2,
            '}' => 3,
            '>' => 4,
            _ => panic!("Unexpected character! {}", ch),
        }
    }
    sum
}

impl Solver for DayTenSolver {
    fn from_input(input: &str) -> Result<Box<Self>, String>
    where
        Self: Sized,
    {
        Ok(Box::new(DayTenSolver {
            lines: input.lines().map(|line| line.to_string()).collect(),
        }))
    }

    fn part_one(&self) -> Result<usize, &str> {
        Ok(self
            .lines
            .iter()
            .map(|line| get_corrupt_line_val(line))
            .sum())
    }

    fn part_two(&self) -> Result<usize, &str> {
        let mut scores = self
            .lines
            .iter()
            .map(|line| get_missing_token_val(line))
            .filter(|&score| score != 0)
            .collect::<Vec<usize>>();
        scores.sort_unstable();
        Ok(scores[scores.len() / 2])
    }
}
