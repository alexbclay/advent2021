use std::collections::HashSet;

use crate::{util::Coord, Solver};

pub struct DayNineSolver {
    map: Vec<Vec<u8>>,
}

impl Solver for DayNineSolver {
    fn from_input(input: &str) -> Result<Box<Self>, String>
    where
        Self: Sized,
    {
        let mut map: Vec<Vec<u8>> = vec![];
        for line in input.lines() {
            let mut cur_line: Vec<u8> = vec![];
            for ch in line.chars() {
                cur_line.push(ch.to_string().parse().unwrap());
            }
            map.push(cur_line);
        }

        Ok(Box::new(DayNineSolver { map }))
    }
    fn part_one(&self) -> Result<usize, &str> {
        let mut risk_level: usize = 0;
        for (row_num, row_vals) in self.map.iter().enumerate() {
            for (col_num, &val) in row_vals.iter().enumerate() {
                let up = if row_num > 0 {
                    self.map[row_num - 1][col_num]
                } else {
                    99
                };
                let down = if row_num < self.map.len() - 1 {
                    self.map[row_num + 1][col_num]
                } else {
                    99
                };
                let left = if col_num > 0 {
                    self.map[row_num][col_num - 1]
                } else {
                    99
                };
                let right = if col_num < row_vals.len() - 1 {
                    self.map[row_num][col_num + 1]
                } else {
                    99
                };

                if val < up && val < down && val < left && val < right {
                    risk_level += 1 + val as usize;
                }
            }
        }
        Ok(risk_level)
    }

    fn part_two(&self) -> Result<usize, &str> {
        /*
        This is basically a flood-fill where the borders are unreachable
        (after removing all the 9s in the grid)
         */
        let mut unseen: HashSet<Coord> = HashSet::new();

        // initialize the unseen coords
        for (row_num, row_vals) in self.map.iter().enumerate() {
            for (col_num, &val) in row_vals.iter().enumerate() {
                if val == 9 {
                    // skip any 9s.  They are the borders
                    continue;
                }
                unseen.insert(Coord {
                    y: row_num as i16,
                    x: col_num as i16,
                });
            }
        }

        let mut filled_sections: Vec<HashSet<Coord>> = vec![];
        let mut seen: HashSet<Coord> = HashSet::new();
        let mut to_check: Vec<Coord> = vec![];
        let mut section: HashSet<Coord> = HashSet::new();
        'outer: loop {
            // we've seen all the spaces now
            if unseen.is_empty() {
                break 'outer;
            }

            // unseen is not empty, but to_check should be here, so we'll add the "next" unseen coord
            to_check.push(*unseen.iter().next().unwrap());
            'section: loop {
                // get the next coord to check
                if let Some(cur_coord) = to_check.pop() {
                    if seen.contains(&cur_coord) {
                        // no need to do anything if we already processed it
                        continue;
                    }
                    // mark this one as seen and remove from the unseen list
                    seen.insert(cur_coord);
                    unseen.remove(&cur_coord);

                    // add to this section's coordinates (not strictly necessary for the puzzle)
                    section.insert(cur_coord);
                    // add its neighbors to the check list
                    for neighbor in cur_coord.get_orthogonal_neighbors() {
                        if unseen.contains(&neighbor) {
                            // if they've been seen, we've already processed them
                            to_check.push(neighbor);
                        }
                    }
                } else {
                    // No more coords to check, so we're done with this section
                    break 'section;
                };
            }
            // We just broke from the inner loop, so we have a full section
            filled_sections.push(section);
            section = HashSet::new();
        }

        let section_sizes =
            filled_sections
                .iter()
                .map(|section| section.len())
                .fold((0, 0, 0), |accum, cur| {
                    println!("{:?} {}", accum, cur);
                    if cur > accum.2 {
                        (accum.1, accum.2, cur)
                    } else if cur > accum.1 {
                        (accum.1, cur, accum.2)
                    } else if cur > accum.0 {
                        (cur, accum.1, accum.2)
                    } else {
                        accum
                    }
                });
        println!("{:?}", section_sizes);
        Ok(section_sizes.0 * section_sizes.1 * section_sizes.2)
    }
}
