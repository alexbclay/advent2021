use std::ops::Range;

use crate::Solver;

#[derive(Debug)]
enum PacketVal {
    Literal(usize),
    SubPacket(Box<Packet>),
}

#[derive(Debug)]
struct Packet {
    version: usize,
    type_id: usize,
    values: Vec<PacketVal>,
}

impl Packet {
    fn get_version_sum(&self) -> usize {
        self.version
            + self
                .values
                .iter()
                .filter_map(|sub_packet| {
                    if let PacketVal::SubPacket(p) = sub_packet {
                        Some(p)
                    } else {
                        None
                    }
                })
                .fold(0, |sum, packet| sum + packet.get_version_sum())
    }

    fn execute(&self) -> usize {
        let executed_vals = self
            .values
            .iter()
            .map(|packval| match packval {
                PacketVal::Literal(val) => *val,
                PacketVal::SubPacket(pack) => pack.execute(),
            })
            .collect::<Vec<usize>>();
        match self.type_id {
            // SUM
            0 => executed_vals.iter().sum(),
            // PRODUCT
            1 => executed_vals.iter().product(),
            // MIN
            2 => *executed_vals.iter().min().unwrap(),
            // MAX
            3 => *executed_vals.iter().max().unwrap(),
            // LITERAL
            4 => executed_vals[0],
            // GREATER THAN
            5 => {
                let lhs = executed_vals[0];
                let rhs = executed_vals[1];
                if lhs > rhs {
                    1
                } else {
                    0
                }
            }
            // LESS THAN
            6 => {
                let lhs = executed_vals[0];
                let rhs = executed_vals[1];
                if lhs < rhs {
                    1
                } else {
                    0
                }
            }
            // EQUAL
            7 => {
                let lhs = executed_vals[0];
                let rhs = executed_vals[1];
                if lhs == rhs {
                    1
                } else {
                    0
                }
            }
            _ => panic!("Unexpected type_id!"),
        }
    }
}

pub struct DaySixteenSolver {
    // TODO: try doing this with larger integer types
    bits: Vec<u8>,
}

fn extract_bits(source: u8, start: u8, end: u8) -> u8 {
    // FIXME: change input from range to start + offset?
    // ...111..
    // 01234567
    // range: 3..6
    // shift right 2 (8-6)
    // mask & 111 2^(6-3) - 1

    let rshift = 8 - end;
    let diff: u32 = (end - start) as u32;
    let pow = 2_u16.pow(diff);
    let mask = (pow - 1) as u8;

    (source >> rshift) & mask
}

impl DaySixteenSolver {
    fn decode_packet(&self, start_index: usize) -> Result<(Packet, usize), String> {
        // println!("== DECODE (from {start_index})");
        let mut cur_index = start_index;

        // first 3 bits indicate packet version
        let packet_version = self.get_bit_slice(cur_index..cur_index + 3)?;
        cur_index += 3;

        // next 3 bits indicate packet type
        let packet_type = self.get_bit_slice(cur_index..cur_index + 3)?;
        cur_index += 3;
        // println!("    PACKET VERSION {packet_version}");
        // println!("    PACKET TYPE {packet_type}");

        if packet_type == 4 {
            // println!("Literal");
            // the rest of this packet is 5 bit chunks
            let mut val: usize = 0;
            loop {
                let next_5 = self.get_bit_slice(cur_index..cur_index + 5)?;
                // println!(
                //     "NEXT 5: {next_5:b} {:b} ({cur_index} {})",
                //     next_5 & 16,
                //     cur_index + 5,
                // );
                val <<= 4;

                cur_index += 5;
                if next_5 & 16 != 16 {
                    val += next_5;
                    // first bit is a 0
                    break;
                } else {
                    val += next_5 - 16;
                }
            }
            // println!("Literal val: {val}");
            Ok((
                Packet {
                    version: packet_version,
                    type_id: packet_type,
                    values: vec![PacketVal::Literal(val)],
                },
                cur_index,
            ))
        } else {
            // println!("Sub packet {cur_index}");
            // non-literal packet type
            let next_bit = self.get_bit_slice(cur_index..cur_index + 1)?;
            cur_index += 1;
            let mut sub_packets: Vec<PacketVal> = vec![];
            if next_bit == 0 {
                // println!("Length type: 0");
                let packet_len = self.get_bit_slice(cur_index..cur_index + 15)?;
                cur_index += 15;
                // println!("{cur_index} {packet_len}");
                // let packet_data = self.get_bit_slice(cur_index..cur_index + packet_len)?;
                // cur_index += packet_len;

                // parse out any packets in the next $packet_len bits
                let packet_end = cur_index + packet_len;
                while let Ok((sub, idx)) = self.decode_packet(cur_index) {
                    sub_packets.push(PacketVal::SubPacket(Box::new(sub)));
                    cur_index = idx;
                    if cur_index >= packet_end {
                        break;
                    }
                }
            } else {
                let packet_count = self.get_bit_slice(cur_index..cur_index + 11)?;
                cur_index += 11;
                for _ in 0..packet_count {
                    let (sub, idx) = self.decode_packet(cur_index)?;
                    cur_index = idx;
                    // println!("{:?}", sub);
                    sub_packets.push(PacketVal::SubPacket(Box::new(sub)));
                }
            }
            Ok((
                Packet {
                    version: packet_version,
                    type_id: packet_type,
                    values: sub_packets,
                },
                cur_index,
            ))
        }
    }

    fn decode(&self) -> Packet {
        // println!("bits: {:?}", self.bits);
        // println!("count: {}", self.bits.len() * 8);

        // println!("{:?}", self.decode_packet(0)?);
        self.decode_packet(0).unwrap().0
    }
    fn get_bit_slice(&self, mut slice: Range<usize>) -> Result<usize, String> {
        // println!("{:?} {:?}", slice.start, slice.end);
        let max_len = self.bits.len() * 8;
        if slice.start >= max_len {
            return Err(format!("Start is out of range: {}", slice.start));
        }
        if slice.end > max_len {
            // println!("GOT TO THE END!");
            slice.end = max_len;
            return Err(format!("End is out of range: {} >= {}", slice.end, max_len));
        }
        let start_vec = slice.start / 8;
        let end_vec = (slice.end - 1) / 8;

        match start_vec.cmp(&end_vec) {
            std::cmp::Ordering::Less => {
                // extract end of the start vec, all middle ones, and the first part of the end one
                let mut val: usize = 0;
                let start: u8 = (slice.start - start_vec * 8) as u8;
                val += extract_bits(self.bits[start_vec], start, 8) as usize;
                // println!("Start: {start_vec} End: {end_vec}");
                // println!("x: {start_vec} val: {val:b}");
                for x in start_vec + 1..end_vec {
                    val <<= 8;
                    val += self.bits[x] as usize;
                }
                // add the last bits
                let end: u8 = (slice.end - end_vec * 8) as u8;
                val <<= slice.end - end_vec * 8;
                val += extract_bits(self.bits[end_vec], 0, end) as usize;
                // println!("x: {end_vec} val: {val:b} {end}");
                Ok(val)
            }
            std::cmp::Ordering::Equal => {
                let num = self.bits[start_vec];
                let start: u8 = (slice.start - start_vec * 8) as u8;
                let end: u8 = (slice.end - start_vec * 8) as u8;
                Ok(extract_bits(num, start, end) as usize)
            }
            std::cmp::Ordering::Greater => unreachable!("Should never have this situation!"),
        }
    }
}

impl Solver for DaySixteenSolver {
    fn from_input(input: &str) -> Result<Box<Self>, String>
    where
        Self: Sized,
    {
        // only one line
        let line = input.lines().next().unwrap();
        if line.len() % 2 != 0 {
            panic!("CANNOT HANDLE ODD STRINGS!!");
        }
        // decode each hex character into bits (stolen from https://stackoverflow.com/a/52992629/6705467)
        let bits: Vec<u8> = (0..line.len())
            .step_by(2)
            .map(|i| u8::from_str_radix(&line[i..i + 2], 16).unwrap())
            .collect();
        Ok(Box::new(DaySixteenSolver { bits }))
    }

    fn part_one(&self) -> Result<usize, &str> {
        let packet = self.decode();
        Ok(packet.get_version_sum())
    }

    fn part_two(&self) -> Result<usize, &str> {
        let packet = self.decode();
        Ok(packet.execute())
    }
}
