use std::collections::HashMap;

use regex::Regex;

use crate::Solver;

#[derive(Copy, Clone, PartialEq, Eq, Hash)]
struct Digit {
    // This could have been done with an 8-bit integer, but I didn't realize that until I was mostly done
    segments: [bool; 7],
}

impl std::fmt::Debug for Digit {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let map = ['a', 'b', 'c', 'd', 'e', 'f', 'g'];
        let mut digit = "".to_string();
        for (i, &is_on) in self.segments.iter().enumerate() {
            if is_on {
                digit.push(map[i]);
            }
        }
        write!(f, "{:?}", digit)
    }
}

impl Digit {
    fn from_string(inp: &str) -> Self {
        let mut segments = [false; 7];
        for ch in inp.chars() {
            segments[match ch {
                'a' => 0,
                'b' => 1,
                'c' => 2,
                'd' => 3,
                'e' => 4,
                'f' => 5,
                'g' => 6,
                _ => panic!("Unexpected character! {}", ch),
            }] = true;
        }
        Digit { segments }
    }

    fn len(&self) -> usize {
        self.segments
            .iter()
            .fold(0, |total, &cur| if cur { total + 1 } else { total })
    }

    /**
     * Return a digit whose segments are different (ie XOR)
     */
    fn _diff(&self, other: &Digit) -> Digit {
        let mut segments = [false; 7];
        for (i, &my_item) in self.segments.iter().enumerate() {
            if my_item != other.segments[i] {
                segments[i] = true
            }
        }
        Digit { segments }
    }

    /**
    Return true if the given digit is a subset of us
     */
    fn contains(&self, other: &Digit) -> bool {
        for (i, &their_item) in other.segments.iter().enumerate() {
            if their_item && !self.segments[i] {
                return false;
            }
        }
        // return true if all of their turned on segments are also on in ours
        true
    }
}

#[derive(Debug)]
struct Entry {
    signal_patterns: [Digit; 10],
    output: [Digit; 4],
}

impl Entry {
    fn decode(&self) -> usize {
        let mut mapping: [Option<Digit>; 10] = [None; 10];
        let mut mapping_2: HashMap<Digit, u8> = HashMap::new();
        let mut is_0_6_or_9: Vec<Digit> = vec![];
        let mut is_2_3_or_5: Vec<Digit> = vec![];
        for &digit in self.signal_patterns.iter() {
            match digit.len() {
                2 => {
                    mapping[1] = Some(digit);
                    mapping_2.insert(digit, 1);
                }
                3 => {
                    mapping[7] = Some(digit);
                    mapping_2.insert(digit, 7);
                }
                4 => {
                    mapping[4] = Some(digit);
                    mapping_2.insert(digit, 4);
                }
                5 => {
                    is_2_3_or_5.push(digit);
                }
                6 => {
                    is_0_6_or_9.push(digit);
                }
                7 => {
                    mapping[8] = Some(digit);
                    mapping_2.insert(digit, 8);
                }
                _ => panic!("Unexpected digit length!"),
            }
        }
        // identify the 3 (only one with 5 segments that contains 1)
        for &digit in is_2_3_or_5.iter() {
            if digit.contains(&mapping[1].unwrap()) {
                mapping[3] = Some(digit);
                mapping_2.insert(digit, 3);
            }
        }

        for &digit in is_0_6_or_9.iter() {
            // identify the 9 (only one with 6 segments that contains 3)
            if digit.contains(&mapping[3].unwrap()) {
                mapping[9] = Some(digit);
                mapping_2.insert(digit, 9);
            } else if digit.contains(&mapping[1].unwrap()) {
                // doesn't contain the 3, but if it contains the 1, then it's 0
                mapping[0] = Some(digit);
                mapping_2.insert(digit, 0);
            } else {
                // does not contain 1 or 3, so it's got to be 6
                mapping[6] = Some(digit);
                mapping_2.insert(digit, 6);
            }
        }

        // figure out the 2 and the 5 (5 fits in 9, 2 does not)
        for &digit in is_2_3_or_5.iter() {
            if digit == mapping[3].unwrap() {
                // already figured this one out
                continue;
            }
            if mapping[9].unwrap().contains(&digit) {
                mapping[5] = Some(digit);
                mapping_2.insert(digit, 5);
            } else {
                mapping[2] = Some(digit);
                mapping_2.insert(digit, 2);
            }
        }
        // Build the solution out of the "output" digits
        let mut solution: usize = 0;
        for &digit in self.output.iter() {
            let val = *mapping_2.get(&digit).unwrap();
            solution *= 10;
            solution += val as usize;
        }
        solution
    }
}

pub struct DayEightSolver {
    entries: Vec<Entry>,
}

impl Solver for DayEightSolver {
    fn from_input(input: &str) -> Result<Box<Self>, String>
    where
        Self: Sized,
    {
        lazy_static! {
            static ref LINE_RE: Regex = Regex::new(r"([a-g]+) ?").unwrap();
        }
        let mut entries: Vec<Entry> = Vec::new();
        for line in input.lines() {
            let mut halves = line.split('|');
            // process the line before the |
            let mut signal_patterns = [Digit {
                segments: [false; 7],
            }; 10];
            for (num, cap) in LINE_RE.captures_iter(halves.next().unwrap()).enumerate() {
                signal_patterns[num] = Digit::from_string(&cap[1]);
            }
            // process the line after the |
            let mut output = [Digit {
                segments: [false; 7],
            }; 4];
            for (num, cap) in LINE_RE.captures_iter(halves.next().unwrap()).enumerate() {
                output[num] = Digit::from_string(&cap[1]);
            }
            entries.push(Entry {
                signal_patterns,
                output,
            });
        }
        Ok(Box::new(DayEightSolver { entries }))
    }

    fn part_one(&self) -> Result<usize, &str> {
        let mut count = 0;
        for entry in self.entries.iter() {
            for digit in entry.output {
                if let 2 | 3 | 4 | 7 = digit.len() {
                    count += 1
                }
            }
        }
        Ok(count)
    }

    fn part_two(&self) -> Result<usize, &str> {
        let mut sum = 0;
        for entry in self.entries.iter() {
            sum += entry.decode();
        }
        Ok(sum)
    }
}
