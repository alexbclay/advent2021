use std::{
    cmp::Ordering,
    collections::{BinaryHeap, HashMap, HashSet},
};

use crate::{util::Coord, Solver};

pub struct DayFifteenSolver {
    edges: HashMap<(Coord, Coord), usize>,
    start: Coord,
    end_part_1: Coord,
    end_part_2: Coord,
}

#[derive(PartialEq, Eq, Debug)]
struct PathCost {
    cost: usize,
    dest: Coord,
}

impl Ord for PathCost {
    fn cmp(&self, other: &Self) -> Ordering {
        other.cost.cmp(&self.cost)
    }
}

impl PartialOrd for PathCost {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl DayFifteenSolver {
    /**
     * Sorta dijkstra with some shortcuts
     */
    fn dijkstra(&self, start: &Coord, end: &Coord) -> usize {
        let mut queue: BinaryHeap<PathCost> = BinaryHeap::new();
        let mut seen: HashSet<Coord> = HashSet::new();
        queue.push(PathCost {
            cost: 0,
            dest: *start,
        });

        while let Some(PathCost { cost, dest }) = queue.pop() {
            if &dest == end {
                return cost;
            }
            if seen.contains(&dest) {
                // already processed this node
                continue;
            }
            seen.insert(dest);
            for neighbor in dest.get_orthogonal_neighbors() {
                // get the edge cost if it's available.  If there's no cost, then who cares?
                if let Some(edge_cost) = self.edges.get(&(dest, neighbor)) {
                    let next_cost = PathCost {
                        cost: cost + edge_cost,
                        dest: neighbor,
                    };
                    queue.push(next_cost);
                }
            }
        }
        panic!("No path found!");
    }
}

impl Solver for DayFifteenSolver {
    fn from_input(input: &str) -> Result<Box<Self>, String>
    where
        Self: Sized,
    {
        // parse lines into an integer grid
        let grid = input
            .lines()
            .map(|line| {
                line.chars()
                    .map(|ch| ch.to_string().parse().unwrap())
                    .collect::<Vec<usize>>()
            })
            .collect::<Vec<Vec<usize>>>();

        // convert to all the edges
        let mut edges = HashMap::new();

        // get all edges for both parts
        let row_len: i16 = grid.len() as i16;
        let col_len: i16 = grid[0].len() as i16;
        for inc_x in 0..5 {
            for inc_y in 0..5 {
                for (row_num, row) in grid.iter().enumerate() {
                    for (col_num, val) in row.iter().enumerate() {
                        let mut value = val + inc_x + inc_y;
                        if value > 9 {
                            value = value % 10 + 1
                        }
                        let dest_coord = Coord {
                            x: col_num as i16 + col_len * inc_x as i16,
                            y: row_num as i16 + row_len * inc_y as i16,
                        };

                        for source_coord in dest_coord.get_orthogonal_neighbors() {
                            edges.insert((source_coord, dest_coord), value);
                        }
                    }
                }
            }
        }

        Ok(Box::new(DayFifteenSolver {
            edges,
            end_part_1: Coord {
                x: col_len - 1,
                y: row_len - 1,
            },
            end_part_2: Coord {
                x: col_len * 5 - 1,
                y: row_len * 5 - 1,
            },
            start: Coord { x: 0, y: 0 },
        }))
    }

    fn part_one(&self) -> Result<usize, &str> {
        Ok(self.dijkstra(&self.start, &self.end_part_1))
    }

    fn part_two(&self) -> Result<usize, &str> {
        Ok(self.dijkstra(&self.start, &self.end_part_2))
    }
}
