use std::collections::HashSet;

use crate::{util::Coord, Solver};

pub struct DayElevenSolver {
    grid: Grid,
}

#[derive(Clone)]
struct Grid {
    matrix: [[u8; 10]; 10],
    lifetime_flashes: usize,
}

impl Grid {
    fn do_step(&self) -> Grid {
        // for each spot, bump the energy value up 1
        // if it goes to 9, add to list to process burst
        let mut bursts: Vec<Coord> = vec![];
        let mut new_grid = self.clone();
        for (row_num, row_vals) in self.matrix.iter().enumerate() {
            for (col_num, &val) in row_vals.iter().enumerate() {
                if val + 1 > 9 {
                    bursts.push(Coord {
                        x: col_num as i16,
                        y: row_num as i16,
                    })
                }
                new_grid.matrix[row_num][col_num] = val + 1;
            }
        }

        // process bursts
        let mut seen: HashSet<Coord> = HashSet::new();
        while let Some(next) = bursts.pop() {
            if seen.contains(&next) {
                // can't burst more than once per step
                continue;
            }
            seen.insert(next);
            new_grid.lifetime_flashes += 1;
            for neighbor in next.get_all_neighbors() {
                let row = neighbor.y;
                let col = neighbor.x;
                if !((0..10).contains(&row) && (0..10).contains(&col)) {
                    // not within our grid bounds
                    continue;
                }
                if seen.contains(&neighbor) {
                    // don't increment or re-burst if this neighbor has already burst
                    continue;
                }
                new_grid.matrix[row as usize][col as usize] += 1;
                if new_grid.matrix[row as usize][col as usize] > 9 {
                    bursts.push(neighbor);
                }
            }
            new_grid.matrix[next.y as usize][next.x as usize] = 0;
        }
        new_grid
    }

    fn is_synchronized(&self) -> bool {
        for row in self.matrix.iter() {
            for &val in row.iter() {
                if val != 0 {
                    return false;
                }
            }
        }
        // every value is 0
        true
    }
}

impl std::fmt::Debug for Grid {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        for &row in self.matrix.iter() {
            for &val in row.iter() {
                write!(f, "{} ", val)?;
            }
            writeln!(f)?;
        }
        writeln!(f, "Lifetime flashes: {}", self.lifetime_flashes)
    }
}

impl Solver for DayElevenSolver {
    fn from_input(input: &str) -> Result<Box<Self>, String>
    where
        Self: Sized,
    {
        let vec_grid = input
            .lines()
            .map(|line| {
                line.chars()
                    .map(|ch| ch.to_string().parse::<u8>().unwrap())
                    .collect::<Vec<u8>>()
            })
            .collect::<Vec<Vec<u8>>>();
        let mut matrix: [[u8; 10]; 10] = [[255; 10]; 10];
        for row in 0..10 {
            for col in 0..10 {
                matrix[row][col] = vec_grid[row][col];
            }
        }
        Ok(Box::new(DayElevenSolver {
            grid: Grid {
                matrix,
                lifetime_flashes: 0,
            },
        }))
    }

    fn part_one(&self) -> Result<usize, &str> {
        let mut p1_grid = self.grid.clone();
        for i in 0..100 {
            p1_grid = p1_grid.do_step();
            println!("{}\n{:?}", i, p1_grid);
        }
        Ok(p1_grid.lifetime_flashes)
    }

    fn part_two(&self) -> Result<usize, &str> {
        let mut p2_grid = self.grid.clone();
        let mut step = 0;
        while !p2_grid.is_synchronized() {
            step += 1;
            p2_grid = p2_grid.do_step();
            println!("{}:\n{:?}", step, p2_grid);
        }
        Ok(step)
    }
}
