use std::{
    collections::{HashMap, HashSet},
    ops::Add,
};

use regex::Regex;

use crate::{util::Coord, Solver};

impl Coord {
    fn get_coords_to(&self, other: &Coord, include_diagonal: bool) -> Vec<Coord> {
        let mut res: Vec<Coord> = Vec::new();
        if self.y == other.y {
            for new_x in self.x.min(other.x)..=self.x.max(other.x) {
                res.push(Coord {
                    x: new_x,
                    y: self.y,
                })
            }
        } else if self.x == other.x {
            for new_y in self.y.min(other.y)..=self.y.max(other.y) {
                res.push(Coord {
                    x: self.x,
                    y: new_y,
                })
            }
        } else if include_diagonal {
            let delta = Coord {
                x: if self.x < other.x { 1 } else { -1 },
                y: if self.y < other.y { 1 } else { -1 },
            };
            let diff = (self.x - other.x).abs();
            for step in 0..=diff {
                res.push(self + &delta.scale(step));
            }
        }
        res
    }

    fn scale(&self, magnitude: i16) -> Coord {
        Coord {
            x: self.x * magnitude,
            y: self.y * magnitude,
        }
    }
}

impl Add for &Coord {
    type Output = Coord;

    fn add(self, rhs: Self) -> Self::Output {
        Coord {
            x: self.x + rhs.x,
            y: self.y + rhs.y,
        }
    }
}

pub struct DayFiveSolver {
    lines: Vec<(Coord, Coord)>,
}

fn draw_grid(spaces: HashMap<Coord, usize>, max_x: i16, max_y: i16) {
    for y in 0..=max_y {
        for x in 0..=max_x {
            match spaces.get(&Coord { x, y }) {
                Some(val) => print!("{}", val),
                None => print!("."),
            }
        }
        println!();
    }
}
impl Solver for DayFiveSolver {
    fn from_input(input: &str) -> Result<Box<Self>, String>
    where
        Self: Sized,
    {
        lazy_static! {
            static ref LINE_RE: Regex = Regex::new(r"(\d+),(\d+) -> (\d+),(\d+)").unwrap();
        }
        Ok(Box::new(DayFiveSolver {
            lines: input
                .lines()
                .map(|line| {
                    let captures = LINE_RE.captures(line).unwrap();
                    (
                        Coord {
                            x: captures[1].parse().unwrap(),
                            y: captures[2].parse().unwrap(),
                        },
                        Coord {
                            x: captures[3].parse().unwrap(),
                            y: captures[4].parse().unwrap(),
                        },
                    )
                })
                .collect::<Vec<(Coord, Coord)>>(),
        }))
    }

    fn part_one(&self) -> Result<usize, &str> {
        let mut danger_zones: HashSet<Coord> = HashSet::new();
        let mut seen: HashSet<Coord> = HashSet::new();
        let mut spaces: HashMap<Coord, usize> = HashMap::new();
        let mut max_x = 0;
        let mut max_y = 0;

        for (start, end) in &self.lines {
            for coord in start.get_coords_to(end, false) {
                max_x = max_x.max(coord.x);
                max_y = max_y.max(coord.y);
                spaces.entry(coord).and_modify(|v| *v += 1).or_insert(1);
                if seen.contains(&coord) {
                    danger_zones.insert(coord);
                } else {
                    seen.insert(coord);
                }
            }
        }
        Ok(danger_zones.len())
    }

    fn part_two(&self) -> Result<usize, &str> {
        let mut danger_zones: HashSet<Coord> = HashSet::new();
        let mut seen: HashSet<Coord> = HashSet::new();
        // NOTE: spaces and max x,y are only necessary for visualizing the map
        let mut spaces: HashMap<Coord, usize> = HashMap::new();
        let mut max_x = 0;
        let mut max_y = 0;

        for (start, end) in &self.lines {
            for coord in start.get_coords_to(end, true) {
                max_x = max_x.max(coord.x);
                max_y = max_y.max(coord.y);
                spaces.entry(coord).and_modify(|v| *v += 1).or_insert(1);
                if seen.contains(&coord) {
                    danger_zones.insert(coord);
                } else {
                    seen.insert(coord);
                }
            }
        }
        draw_grid(spaces, max_x.min(100), max_y.min(100));
        Ok(danger_zones.len())
    }
}
