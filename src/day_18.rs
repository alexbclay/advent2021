use crate::Solver;
use std::collections::VecDeque;

use std::ops::Add;
use std::sync::atomic::{AtomicUsize, Ordering::SeqCst};
use std::{collections::HashMap, fmt::Display};

static NEXT_ID: AtomicUsize = AtomicUsize::new(0);
fn get_id() -> usize {
    NEXT_ID.fetch_add(1, SeqCst)
}

#[derive(Clone, Debug)]
enum Element {
    // the value of the leaf (0-9)
    Leaf(usize),
    // the index of each of the map.  Each element has a GLOBALLY unique ID (this is not a great design)
    Pair(usize, usize),
}

pub struct DayEighteenSolver {
    numbers: Vec<SnailFishNumber>,
}

#[derive(Debug)]
struct RenameMe {
    key: usize,
    value: usize,
    depth: usize,
    parent: usize,
}

#[derive(Clone)]
struct SnailFishNumber {
    map: HashMap<usize, Element>,
    root: usize,
}

impl SnailFishNumber {
    /// Creates an empty SNF for use in testing and folds
    fn empty() -> SnailFishNumber {
        SnailFishNumber {
            map: HashMap::new(),
            root: 0,
        }
    }

    /// Display the SNF in a fish-readable way
    fn display_element(&self, f: &mut std::fmt::Formatter<'_>, index: usize) -> std::fmt::Result {
        if self.map.is_empty() {
            return write!(f, "EMPTY");
        }
        let el = self.map.get(&index).unwrap();
        match el {
            Element::Leaf(num) => write! {f, "{}", num},
            Element::Pair(lhs_index, rhs_index) => {
                write!(f, "[")?;
                self.display_element(f, *lhs_index)?;
                write!(f, ",")?;
                self.display_element(f, *rhs_index)?;
                write!(f, "]")
            }
        }
    }

    /// Returns a SnailFishNumber
    fn from_str(value: &str) -> SnailFishNumber {
        let mut root: usize = 0;
        let mut map = HashMap::new();
        let mut stack: Vec<(Option<usize>, Option<usize>)> = vec![];
        for ch in value.chars() {
            match ch {
                // new pair starting!
                '[' => {
                    stack.push((None, None));
                }
                // Leaf node == single digit
                '0' | '1' | '2' | '3' | '4' | '5' | '6' | '7' | '8' | '9' => {
                    // parse the leaf and insert it into the map with a name of `next`
                    let leaf = Element::Leaf(ch.to_string().parse().unwrap());
                    let leaf_name = get_id();
                    map.insert(leaf_name, leaf);
                    let prev = stack.pop().unwrap();
                    // this is the lhs of a pair on the stack
                    if prev.0.is_none() {
                        stack.push((Some(leaf_name), None))
                    } else {
                        // this is the rhs of a pair on the stack
                        stack.push((prev.0, Some(leaf_name)));
                    }
                }
                // finish the current pair
                ']' => {
                    let new_pair_name = get_id();
                    if let (Some(lhs), Some(rhs)) = stack.pop().unwrap() {
                        map.insert(new_pair_name, Element::Pair(lhs, rhs));
                        // if there are no more pairs left, that means this is the root node
                        // but we'll end anyways since we're not validating the input
                        if stack.is_empty() {
                            root = new_pair_name;
                            continue;
                        }
                        let prev = stack.pop().unwrap();
                        // this is the lhs of a pair on the stack
                        if prev.0.is_none() {
                            stack.push((Some(new_pair_name), None))
                        } else {
                            // this is the rhs of a pair on the stack
                            stack.push((prev.0, Some(new_pair_name)));
                        }
                    } else {
                        panic!("INCOMPLETE PAIR!");
                    }
                }
                _ => {
                    continue;
                }
            };
        }
        SnailFishNumber { map, root }
    }

    /// Calculates the ordered list of leaf nodes and returns then along with their keys and their depths
    fn get_leaf_list(&self) -> Vec<RenameMe> {
        let mut res_list = vec![];

        let mut queue: VecDeque<(usize, usize, usize)> = VecDeque::from([(self.root, 0, 0)]);

        while let Some((cur_node, cur_depth, parent)) = queue.pop_front() {
            match self.map.get(&cur_node) {
                Some(Element::Pair(lhs, rhs)) => {
                    // it's a pair, so push its children onto the queue
                    queue.push_front((*rhs, cur_depth + 1, cur_node));
                    queue.push_front((*lhs, cur_depth + 1, cur_node));
                }
                Some(Element::Leaf(value)) => {
                    // it's a leaf, so push it onto the result list
                    res_list.push(RenameMe {
                        key: cur_node,
                        value: *value,
                        depth: cur_depth,
                        parent,
                    })
                }
                None => panic!("Graph is broken!  A non-existent node was referred to by another one.  Parent: {parent} Missing node: {cur_node}"),
            }
        }

        res_list
    }

    /// Apply rules, but only one can change
    /// Rules:
    /// 1. if any element is 4 deep, then explode it
    /// 2. if any leaf is 10+, then split it
    fn reduce(&mut self) -> bool {
        self.explode_deep() || self.split_large()
    }

    /// Check the max depth on elements
    fn explode_deep(&mut self) -> bool {
        let leaf_list = self.get_leaf_list();

        let mut leaf_index = usize::MAX;
        for (i, info) in leaf_list.iter().enumerate() {
            if info.depth >= 5 {
                leaf_index = i;
                break;
            }
        }
        if leaf_index == usize::MAX {
            // no nodes were deep enough to explode
            return false;
        }
        // this is the lhs of the pair, since we're search from left to right
        let left_info = &leaf_list[leaf_index];
        let right_info = &leaf_list[leaf_index + 1];

        if leaf_index > 0 {
            // there's something to the left we can update
            let lefter_info = &leaf_list[leaf_index - 1];
            self.map.insert(
                lefter_info.key,
                Element::Leaf(lefter_info.value + left_info.value),
            );
        }

        if leaf_index < leaf_list.len() - 2 {
            // there's something to the right of our rhs leaf we can update
            let righter_info = &leaf_list[leaf_index + 2];
            self.map.insert(
                righter_info.key,
                Element::Leaf(righter_info.value + right_info.value),
            );
        }
        self.map.insert(left_info.parent, Element::Leaf(0));

        true
    }

    fn split_large(&mut self) -> bool {
        // find the leftmost leaf greater than or equal to 10
        let leaf_list = self.get_leaf_list();
        let mut leaf_index = usize::MAX;
        for (i, info) in leaf_list.iter().enumerate() {
            if info.value >= 10 {
                leaf_index = i;
                break;
            }
        }
        if leaf_index == usize::MAX {
            // no nodes were deep enough to explode
            return false;
        }
        let info = &leaf_list[leaf_index];

        // add two new nodes, then update the old one to be a pair
        let lhs_id = get_id();
        let rhs_id = get_id();

        self.map.insert(lhs_id, Element::Leaf(info.value / 2));
        self.map
            .insert(rhs_id, Element::Leaf(info.value - (info.value / 2)));
        self.map.insert(info.key, Element::Pair(lhs_id, rhs_id));
        true
    }

    fn calc_magnitude(&self, node: usize) -> usize {
        match self.map.get(&node) {
            Some(Element::Leaf(val)) => *val,
            Some(Element::Pair(lhs, rhs)) => {
                self.calc_magnitude(*lhs) * 3 + self.calc_magnitude(*rhs) * 2
            }
            None => panic!("Cannot calc magnitude! {node} is not in the map"),
        }
    }
}

impl Display for SnailFishNumber {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        self.display_element(f, self.root)
    }
}

impl Add for &SnailFishNumber {
    type Output = SnailFishNumber;

    fn add(self, rhs: Self) -> Self::Output {
        // empty + y == y
        if self.map.is_empty() {
            return rhs.clone();
        }
        // x + empty == x
        if rhs.map.is_empty() {
            return self.clone();
        }

        // make a new map for the result number and load it with our IDs and the RHS IDs
        let mut new_map = self.map.clone();
        for (id, val) in &rhs.map {
            new_map.insert(*id, val.clone());
        }
        // make a new node that combines the roots of each of the inputs: x + y = [x, y]
        let next_node_id = get_id();
        new_map.insert(next_node_id, Element::Pair(self.root, rhs.root));

        // make that new node the root of the result number
        let mut new_num = SnailFishNumber {
            map: new_map,
            root: next_node_id,
        };

        // apply reduce and explode rules until they can no longer be applied
        loop {
            let changed = new_num.reduce();
            if !changed {
                break;
            }
        }
        new_num
    }
}

impl Solver for DayEighteenSolver {
    fn from_input(input: &str) -> Result<Box<Self>, String>
    where
        Self: Sized,
    {
        println!("--- FROM INPUT ---");
        let numbers = input
            .lines()
            .map(|line| {
                let num = SnailFishNumber::from_str(line);
                println!("num: {num}");
                num
            })
            .collect::<Vec<SnailFishNumber>>();
        println!("--- DONE INPUT ---");
        Ok(Box::new(DayEighteenSolver { numbers }))
    }

    fn part_one(&self) -> Result<usize, &str> {
        let sum: SnailFishNumber = self
            .numbers
            .iter()
            .fold(SnailFishNumber::empty(), |prev, cur| &prev + cur);
        println!("{sum}");
        Ok(sum.calc_magnitude(sum.root))
    }

    fn part_two(&self) -> Result<usize, &str> {
        let mut max = 0;
        for a in self.numbers.iter() {
            for b in self.numbers.iter() {
                let sum = a + b;
                let mag = sum.calc_magnitude(sum.root);
                if mag > max {
                    max = mag;
                }
            }
        }
        Ok(max)
    }
}
