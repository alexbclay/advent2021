#[macro_use]
extern crate lazy_static;
extern crate regex;

pub trait Solver {
    fn from_input(input: &str) -> Result<Box<Self>, String>
    where
        Self: Sized;

    fn part_one(&self) -> Result<usize, &str> {
        Err("Not implemented yet!")
    }
    fn part_two(&self) -> Result<usize, &str> {
        Err("Not implemented yet!")
    }
}

pub mod day_1;
pub mod day_10;
pub mod day_11;
pub mod day_12;
pub mod day_12_faster;
pub mod day_13;
pub mod day_14;
pub mod day_15;
pub mod day_16;
pub mod day_17;
pub mod day_18;
pub mod day_2;
pub mod day_3;
pub mod day_4;
pub mod day_5;
pub mod day_6;
pub mod day_7;
pub mod day_8;
pub mod day_9;
pub mod util;
pub mod day_19 {}
pub mod day_20 {}
pub mod day_21 {}
pub mod day_22 {}
pub mod day_23 {}
pub mod day_24 {}
pub mod day_25 {}
