use crate::Solver;

pub struct DaySevenSolver {
    crabs: Vec<usize>,
    max: usize,
}

impl DaySevenSolver {
    fn get_smallest_fuel<F>(&self, func: F) -> (usize, usize)
    where
        F: Fn(usize, usize) -> isize,
    {
        (0..=self.max)
            .map(|num| {
                (
                    num,
                    self.crabs
                        .iter()
                        .map(|&crab| func(crab, num) as usize)
                        .sum::<usize>(),
                )
            })
            .reduce(|accum, cur| if accum.1 < cur.1 { accum } else { cur })
            .unwrap()
    }
}

impl Solver for DaySevenSolver {
    fn from_input(input: &str) -> Result<Box<Self>, String>
    where
        Self: Sized,
    {
        let line = input.lines().next().unwrap();
        let mut crabs: Vec<usize> = Vec::new();
        let mut max: usize = 0;

        for val in line.split(',') {
            let crab_num = val.parse().unwrap();
            max = max.max(crab_num);
            crabs.push(crab_num);
        }
        Ok(Box::new(DaySevenSolver { crabs, max }))
    }
    fn part_one(&self) -> Result<usize, &str> {
        let (align, fuel) = self.get_smallest_fuel(|from, to| (from as isize - to as isize).abs());
        println!("alignment: {}  fuel: {}", align, fuel);
        Ok(fuel)
    }

    fn part_two(&self) -> Result<usize, &str> {
        let mut triangle_nums: Vec<isize> = vec![0; self.max + 1];
        for index in 0..triangle_nums.len() {
            triangle_nums[index] = if index == 0 {
                0
            } else {
                triangle_nums[index - 1] + index as isize
            };
        }
        let (align, fuel) = self.get_smallest_fuel(|from, to| {
            triangle_nums[(from as isize - to as isize).abs() as usize]
        });
        println!("alignment: {} fuel: {}", align, fuel);
        Ok(fuel)
    }
}
