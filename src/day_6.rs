use crate::Solver;

struct LanternFishStates {
    by_state: [usize; 9],
}

impl LanternFishStates {
    fn do_generation(&mut self) {
        // fish with a 0 timer go to 6, but make 8s also
        let new_fishes = self.by_state[0];

        for idx in 1..9 {
            self.by_state[idx - 1] = self.by_state[idx];
        }
        self.by_state[6] += new_fishes;
        self.by_state[8] = new_fishes;
    }

    fn do_many_generations(&mut self, x: usize) {
        for _ in 0..x {
            self.do_generation()
        }
    }
}

pub struct DaySixSolver {
    inital_state: Vec<usize>,
}
impl DaySixSolver {
    fn get_fish_states(&self) -> LanternFishStates {
        let mut by_state = [0; 9];
        for state in &self.inital_state {
            by_state[*state] += 1;
        }
        LanternFishStates { by_state }
    }
}

impl Solver for DaySixSolver {
    fn from_input(input: &str) -> Result<Box<Self>, String>
    where
        Self: Sized,
    {
        // only one line of input
        let line = input.lines().next().unwrap();
        Ok(Box::new(DaySixSolver {
            inital_state: line
                .split(',')
                .map(|num| num.parse().unwrap())
                .collect::<Vec<usize>>(),
        }))
    }

    fn part_one(&self) -> Result<usize, &str> {
        let mut pt_one = self.get_fish_states();
        pt_one.do_many_generations(80);
        Ok(pt_one.by_state.iter().sum())
    }

    fn part_two(&self) -> Result<usize, &str> {
        let mut pt_two = self.get_fish_states();
        pt_two.do_many_generations(256);
        Ok(pt_two.by_state.iter().sum())
    }
}
