use crate::Solver;

pub struct DayTwoSolver {
    commands: Vec<Command>,
}

#[derive(Debug)]
enum Command {
    Forward(usize),
    Down(usize),
    Up(usize),
    None,
}

impl Solver for DayTwoSolver {
    fn from_input(input: &str) -> Result<Box<Self>, String>
    where
        Self: Sized,
    {
        let mut commands: Vec<Command> = Vec::new();

        for line in input.lines() {
            let mut line_parts = line.split_whitespace();

            let dir = line_parts.next().unwrap();
            let size: usize = match line_parts.next().unwrap().parse() {
                Ok(it) => it,
                Err(err) => panic!("{}", err),
            };

            commands.push(match dir {
                "forward" => Command::Forward(size),
                "up" => Command::Up(size),
                "down" => Command::Down(size),
                _ => Command::None,
            });
        }
        Ok(Box::new(DayTwoSolver { commands }))
    }
    fn part_one(&self) -> Result<usize, &str> {
        let (x, y): (isize, isize) = self
            .commands
            .iter()
            .map(|c| -> (isize, isize) {
                match c {
                    Command::Forward(x) => (*x as isize, 0),
                    Command::Down(x) => (0, *x as isize),
                    Command::Up(x) => (0, 0 - *x as isize),
                    Command::None => (0, 0),
                }
            })
            .reduce(|prev, cur| (prev.0 + cur.0, prev.1 + cur.1))
            .unwrap();
        Ok(x as usize * y as usize)
    }

    fn part_two(&self) -> Result<usize, &str> {
        let (x, y, _aim): (isize, isize, isize) = self
            .commands
            .iter()
            .map(|c| -> (isize, isize, isize) {
                match c {
                    Command::Forward(x) => (*x as isize, 0, 0),
                    Command::Down(x) => (0, 0, *x as isize),
                    Command::Up(x) => (0, 0, 0 - *x as isize),
                    Command::None => (0, 0, 0),
                }
            })
            .reduce(|prev, cur| (prev.0 + cur.0, prev.1 + (cur.0 * prev.2), prev.2 + cur.2))
            .unwrap();
        Ok(x as usize * y as usize)
    }
}
