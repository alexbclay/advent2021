use regex::Regex;

use crate::{util::Coord, Solver};

pub struct DaySeventeenSolver {
    target_min: Coord,
    target_max: Coord,
}

impl DaySeventeenSolver {
    fn in_target(&self, coord: Coord) -> bool {
        (self.target_min.x..=self.target_max.x).contains(&coord.x)
            && (self.target_min.y..=self.target_max.y).contains(&coord.y)
    }

    fn fire_solution(&self, velocity: &Coord) -> Option<Coord> {
        let mut delta = *velocity;
        let mut cur_step = Coord::new(0, 0);
        let mut highest = Coord::new(0, 0);
        loop {
            cur_step += delta;
            if cur_step.y > highest.y {
                highest = cur_step
            }

            if self.in_target(cur_step) {
                return Some(highest);
            }
            delta.x += match delta.x {
                x if x < 0 => 1,
                x if x > 0 => -1,
                _ => 0,
            };
            delta.y -= 1;
            if cur_step.x > self.target_max.x
                || cur_step.y < self.target_min.y
                || (delta.x == 0 && !(self.target_min.x..=self.target_max.x).contains(&cur_step.x))
            {
                break;
            }
        }
        None
    }
}

impl Solver for DaySeventeenSolver {
    fn from_input(input: &str) -> Result<Box<Self>, String>
    where
        Self: Sized,
    {
        lazy_static! {
            static ref LINE_RE: Regex =
                Regex::new(r"target area: x=(-?\d+)..(-?\d+), y=(-?\d+)..(-?\d+)").unwrap();
        }
        // only one line
        let line = input.lines().next().unwrap();
        let captures = LINE_RE.captures(line).unwrap();

        Ok(Box::new(DaySeventeenSolver {
            target_min: Coord {
                x: captures[1].parse().unwrap(),
                y: captures[3].parse().unwrap(),
            },
            target_max: Coord {
                x: captures[2].parse().unwrap(),
                y: captures[4].parse().unwrap(),
            },
        }))
    }

    fn part_one(&self) -> Result<usize, &str> {
        let mut max = 0;
        for x in 0..self.target_max.x {
            for y in 0..self.target_min.y.abs() {
                let velocity = Coord::new(x, y);
                if let Some(highest) = self.fire_solution(&velocity) {
                    max = max.max(highest.y);
                }
            }
        }
        Ok(max as usize)
    }

    fn part_two(&self) -> Result<usize, &str> {
        let mut count = 0;
        for x in 0..=self.target_max.x {
            for y in (-self.target_min.y.abs())..=self.target_min.y.abs() {
                let velocity = Coord::new(x, y);
                if self.fire_solution(&velocity).is_some() {
                    count += 1;
                }
            }
        }
        Ok(count)
    }
}
