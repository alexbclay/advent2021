pub struct DayOneSolver {
    depths: Vec<usize>,
}

impl crate::Solver for DayOneSolver {
    fn from_input(input: &str) -> Result<Box<Self>, String>
    where
        Self: Sized,
    {
        let mut depths: Vec<usize> = Vec::new();
        for line in input.lines() {
            depths.push(match line.parse() {
                Ok(val) => val,
                Err(err) => return Err(err.to_string()),
            })
        }
        Ok(Box::new(DayOneSolver { depths }))
    }

    fn part_one(&self) -> Result<usize, &str> {
        let increases: usize = self
            .depths
            .iter()
            .zip(self.depths.iter().skip(1))
            .map(|(x, y)| if x < y { 1 } else { 0 })
            .sum();
        Ok(increases)
    }

    fn part_two(&self) -> Result<usize, &str> {
        let increases = self
            .depths
            .iter()
            .zip(self.depths.iter().skip(3))
            .map(|(x, y)| if x < y { 1 } else { 0 })
            .sum();

        Ok(increases)
    }
}
