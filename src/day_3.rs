use crate::Solver;

pub struct DayThreeSolver {
    size: usize,
    lines: Vec<usize>,
}

impl DayThreeSolver {
    fn filter(&self, list: &[usize], digit: usize, greater: bool) -> Vec<usize> {
        // oxygen generator rating: keep most common value
        let (ones, zeros) =
            list.iter()
                .fold((Vec::new(), Vec::new()), |(mut ones, mut zeros), cur| {
                    // println!("{:b} {:b} {:b}", digit, cur, *cur & digit);
                    if *cur & digit == digit {
                        ones.push(*cur)
                    } else {
                        zeros.push(*cur)
                    }
                    (ones, zeros)
                });

        if (ones.len() >= zeros.len() && greater) || (zeros.len() > ones.len() && !greater) {
            ones
        } else {
            zeros
        }
    }
}

impl Solver for DayThreeSolver {
    fn from_input(input: &str) -> Result<Box<Self>, String>
    where
        Self: Sized,
    {
        let mut lines: Vec<usize> = Vec::new();
        let mut size: usize = 0;
        for line in input.lines() {
            lines.push(
                usize::from_str_radix(line, 2).expect("Could not convert binary to unsigned int"),
            );
            size = line.len();
        }
        Ok(Box::new(DayThreeSolver { size, lines }))
    }

    fn part_one(&self) -> Result<usize, &str> {
        // Count up all the instances of 1 in the binary strings
        let totals = self.lines.iter().fold(vec![0; self.size], |mut prev, cur| {
            let mut digits_remaining = self.size;
            let mut cur_int = *cur;
            while digits_remaining > 0 {
                digits_remaining -= 1;
                if cur_int & 1 == 1 {
                    prev[digits_remaining] += 1
                };

                cur_int >>= 1;
            }
            prev
        });
        // conver the final string to an integer
        let gamma: usize = usize::from_str_radix(
            &totals
                .iter()
                // turn the counts back into binary digits if there's more than half 1s
                .map(|x| {
                    if x > &(self.lines.len() / 2) {
                        '1'
                    } else {
                        '0'
                    }
                })
                // turn that into a string
                .collect::<String>(),
            2,
        )
        .expect("Should not ever panic here");

        // calculate the binary complement
        let total: usize = usize::pow(2, self.size as u32) - 1;

        // multiply together
        Ok(gamma * (total ^ gamma))
    }

    fn part_two(&self) -> Result<usize, &str> {
        let mut digit: usize = usize::pow(2, self.size as u32 - 1);
        let mut oxy_lines = self.filter(&self.lines, digit, true);
        loop {
            // println!("OXY: {:b}: {:?}", digit, oxy_lines);
            digit /= 2;
            oxy_lines = self.filter(&oxy_lines, digit, true);
            if oxy_lines.len() == 1 {
                break;
            }
            if digit == 1 {
                break;
            }
        }
        let mut digit: usize = usize::pow(2, self.size as u32 - 1);
        let mut co2_lines = self.filter(&self.lines, digit, false);
        loop {
            // println!("CO2: {:b}: {:?}", digit, co2_lines);
            digit /= 2;
            co2_lines = self.filter(&co2_lines, digit, false);
            if co2_lines.len() == 1 {
                break;
            }
            if digit == 1 {
                break;
            }
        }

        Ok(oxy_lines[0] * co2_lines[0])
    }
}
