use std::collections::HashSet;

use crate::{util::Coord, Solver};
use regex::Regex;

#[derive(Debug)]
enum Axis {
    X,
    Y,
}

#[derive(Debug)]
pub struct DayThirteenSolver {
    dots: HashSet<Coord>,
    folds: Vec<(Axis, usize)>,
}

fn fold(fold_loc: &usize, fold_axis: &Axis, coords: &HashSet<Coord>) -> HashSet<Coord> {
    let mut folded: HashSet<Coord> = HashSet::new();

    for coord in coords.iter() {
        // println!("COORD: {:?}", coord);
        let mut new_coord = *coord;
        match fold_axis {
            Axis::Y => {
                if coord.y as usize > *fold_loc {
                    // larger than the fold, so reduce by the difference
                    new_coord.y = *fold_loc as i16 - (coord.y - *fold_loc as i16)
                }
            }
            Axis::X => {
                if coord.x as usize > *fold_loc {
                    // larger than the fold, so reduce by the difference
                    new_coord.x = *fold_loc as i16 - (coord.x - *fold_loc as i16)
                }
            }
        }
        folded.insert(new_coord);
    }
    folded
}

impl DayThirteenSolver {
    fn fold_all(&self) -> HashSet<Coord> {
        let mut cur_coords = self.dots.clone();
        for (fold_axis, fold_loc) in self.folds.iter() {
            cur_coords = fold(fold_loc, fold_axis, &cur_coords);
        }
        cur_coords
    }
}

impl Solver for DayThirteenSolver {
    fn from_input(input: &str) -> Result<Box<Self>, String>
    where
        Self: Sized,
    {
        let mut dots: HashSet<Coord> = HashSet::new();
        let mut folds: Vec<(Axis, usize)> = vec![];

        let mut line_iter = input.lines();
        lazy_static! {
            static ref FOLD_RE: Regex = Regex::new(r"fold along (x|y)=(\d+)").unwrap();
        }

        for line in &mut line_iter {
            if line.is_empty() {
                // empty line indicates transition between coords and folds
                break;
            }
            dots.insert(Coord::try_from(line).unwrap());
        }

        for line in line_iter {
            let captures = FOLD_RE.captures(line).unwrap();
            folds.push((
                if captures[1].contains('x') {
                    Axis::X
                } else {
                    Axis::Y
                },
                captures[2].parse().unwrap(),
            ))
        }
        Ok(Box::new(DayThirteenSolver { dots, folds }))
    }

    fn part_one(&self) -> Result<usize, &str> {
        let (fold_axis, fold_loc) = &self.folds[0];
        Ok(fold(fold_loc, fold_axis, &self.dots).len())
    }

    fn part_two(&self) -> Result<usize, &str> {
        let folded = self.fold_all();
        Coord::draw_coord_window(Coord { x: 0, y: 0 }, Coord { x: 41, y: 6 }, folded.iter());
        Ok(2)
    }
}
