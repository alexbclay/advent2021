use std::collections::{HashMap, HashSet};

use crate::Solver;

pub struct DayTwelveSolver {
    graph: HashMap<isize, HashSet<isize>>,
}

impl DayTwelveSolver {
    fn count_all_paths(&self, part_one: bool) -> usize {
        // intialize the visit count
        let max_small_cave_index: usize = self
            .graph
            .keys()
            .filter_map(|&key| if key > 0 { Some(key as usize) } else { None })
            .max()
            .unwrap()
            + 1;
        let visited_count = vec![0; max_small_cave_index];

        self.count_paths_from(0, part_one, visited_count, true, 0)
    }
    fn count_paths_from(
        &self,
        node: isize,
        part_one: bool,
        visited_count: Vec<usize>,
        double_allowed: bool,
        depth: usize,
    ) -> usize {
        // for _ in 0..depth {
        //     print!("  ")
        // }
        // println!("{:?} {:?} {}", node, visited_count, double_allowed);

        // BASE CASE: we reached the end, so this counts as a path
        if node == -1 {
            return 1;
        }

        let mut visits = 0;
        if node >= 0 {
            visits = visited_count[node as usize];
        }
        // BASE CASE: we are going back to the start node
        if node == 0 && visits >= 1 {
            return 0;
        }

        // BASE CASE: we are visiting a small node for the second time and it might not be allowed
        if node > 0 && visits >= 1 {
            if part_one {
                // can't visit small nodes more than once in part 1
                return 0;
            }
            // can only visit small nodes more than once if we haven't yet tripped the flag
            if !double_allowed {
                return 0;
            }
        }

        // RECURSIVE CASE: Add up all the paths from each child node and return that
        let mut new_visited_count = visited_count;
        let mut new_double_allowed = double_allowed;
        if node >= 0 {
            // tell child calls that this node has now been visited again
            new_visited_count[node as usize] = visits + 1;
            // tell child calls whether we have double visited a node already (only for part 2)
            new_double_allowed = new_double_allowed && (visits + 1) < 2
        }

        let mut sub_path_count = 0;

        for &child in self.graph.get(&node).unwrap().iter() {
            sub_path_count += self.count_paths_from(
                child,
                part_one,
                new_visited_count.clone(),
                new_double_allowed,
                depth + 1,
            );
        }
        sub_path_count
    }
}

impl Solver for DayTwelveSolver {
    fn from_input(input: &str) -> Result<Box<Self>, String>
    where
        Self: Sized,
    {
        // start node is always 0
        // end node is always -1
        // large nodes are negative
        // small nodes are positive so we can use them as indexes
        let mut cur_small = 0;
        let mut cur_big = -1;
        let mut node_map: HashMap<String, isize> = HashMap::new();
        let mut graph = HashMap::new();
        let mut to_integer = |node: &str| -> isize {
            let int_val = if let Some(val) = node_map.get(node) {
                *val
            } else if node == "start" {
                0
            } else if node == "end" {
                -1
            } else if node.to_uppercase() == node {
                cur_big -= 1;
                cur_big
            } else {
                cur_small += 1;
                cur_small
            };

            node_map.insert(node.to_string(), int_val);
            int_val
        };
        input
            .lines()
            .map(|line| {
                let mut split = line.split('-');
                let name_1 = split.next().unwrap();
                let name_2 = split.next().unwrap();
                (name_1, name_2)
            })
            .for_each(|(a, b)| {
                let node_a = to_integer(a);
                let node_b = to_integer(b);
                graph
                    .entry(node_a)
                    .or_insert_with(HashSet::new)
                    .insert(node_b);
                graph
                    .entry(node_b)
                    .or_insert_with(HashSet::new)
                    .insert(node_a);
            });
        println!("{:?}", node_map);
        println!("{:?}", graph);
        Ok(Box::new(DayTwelveSolver { graph }))
    }

    fn part_one(&self) -> Result<usize, &str> {
        Ok(self.count_all_paths(true))
    }

    fn part_two(&self) -> Result<usize, &str> {
        Ok(self.count_all_paths(false))
    }
}
