use std::{
    collections::{HashMap, HashSet},
    rc::Rc,
};

use crate::Solver;

pub struct DayTwelveSolver {
    graph: Graph,
}

impl Solver for DayTwelveSolver {
    fn from_input(input: &str) -> Result<Box<Self>, String>
    where
        Self: Sized,
    {
        let mut start_node = None;
        let mut edges: HashMap<Rc<Node>, HashSet<Rc<Node>>> = HashMap::new();
        input
            .lines()
            .map(|line| {
                let mut split = line.split('-');
                let name_1 = split.next().unwrap();
                let name_2 = split.next().unwrap();
                (name_1, name_2)
            })
            .for_each(|(a, b)| {
                let node_a = Rc::new(Node::new(a));
                if node_a.node_type == NodeType::Start {
                    start_node = Some(node_a.clone());
                }
                let node_b = Rc::new(Node::new(b));
                if node_b.node_type == NodeType::Start {
                    start_node = Some(node_b.clone());
                }

                // insert connection in both directions
                let edges_a = edges.entry(node_a.clone()).or_insert_with(HashSet::new);
                edges_a.insert(node_b.clone());
                let edges_b = edges.entry(node_b).or_insert_with(HashSet::new);
                edges_b.insert(node_a);
            });

        // find the start node
        if let Some(start) = start_node {
            Ok(Box::new(DayTwelveSolver {
                graph: Graph { start, edges },
            }))
        } else {
            Err("Could not find start/end node".to_string())
        }
    }

    fn part_one(&self) -> Result<usize, &str> {
        let paths = self.graph.find_all_paths(1);
        // for path in paths.iter() {
        //     println!("{:?}", path);
        // }
        Ok(paths.len())
    }

    fn part_two(&self) -> Result<usize, &str> {
        let paths = self.graph.find_all_paths(2);
        // for path in paths.iter() {
        //     println!("{:?}", path);
        // }
        Ok(paths.len())
    }
}

struct Graph {
    start: Rc<Node>,
    edges: HashMap<Rc<Node>, HashSet<Rc<Node>>>,
}

impl Graph {
    fn find_all_paths(&self, small_visit_limit: usize) -> Vec<Vec<Rc<Node>>> {
        self.find_all_paths_from(&self.start, HashMap::new(), small_visit_limit)
    }

    fn find_all_paths_from(
        &self,
        node: &Rc<Node>,
        use_count: HashMap<Rc<Node>, usize>,
        small_visit_limit: usize,
    ) -> Vec<Vec<Rc<Node>>> {
        if node.node_type == NodeType::End {
            return vec![vec![node.clone()]];
        }
        let mut paths: Vec<Vec<Rc<Node>>> = vec![];

        for child in self.edges.get(node).unwrap().iter() {
            let mut new_use_count = use_count.clone();

            // check to see if this child is a valid place to move:
            if child.node_type == NodeType::Start {
                // start is never valid
                continue;
            }
            if child.node_type == NodeType::Small {
                // small nodes need to check if they are acceptable
                // check if there have already been enough small node visits
                if use_count.contains_key(child)
                    && use_count.values().max().unwrap() >= &small_visit_limit
                {
                    continue;
                }
                *new_use_count.entry(child.clone()).or_insert(0) += 1;
            }
            // all other types of child nodes are fine to proceed with

            let sub_paths = self.find_all_paths_from(child, new_use_count, small_visit_limit);
            for path in sub_paths {
                paths.push(vec![vec![node.clone()], path].concat());
            }
        }
        paths
    }
}

#[derive(Debug, Hash, PartialEq, Eq, Clone)]
enum NodeType {
    Start,
    Big,
    Small,
    End,
}

#[derive(Hash, PartialEq, Eq, Clone)]
struct Node {
    node_type: NodeType,
    name: String,
}

impl Node {
    fn new(name: &str) -> Node {
        Node {
            name: name.to_string(),
            node_type: if name == "start" {
                NodeType::Start
            } else if name == "end" {
                NodeType::End
            } else if name.to_uppercase() == name {
                NodeType::Big
            } else {
                NodeType::Small
            },
        }
    }
}

impl std::fmt::Debug for Node {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self.node_type {
            NodeType::Start => write!(f, "[START]"),
            NodeType::Big => write!(f, "B({})", self.name),
            NodeType::Small => write!(f, "s({})", self.name),
            NodeType::End => write!(f, "[END]"),
        }
    }
}
