use std::collections::HashMap;

use regex::Regex;

use crate::Solver;
type Pair = (char, char);

pub struct DayFourteenSolver {
    rules: HashMap<Pair, [Pair; 2]>,
    start_state: HashMap<Pair, usize>,
    last_char: char,
}

fn polymerize(
    state: &HashMap<Pair, usize>,
    rules: &HashMap<Pair, [Pair; 2]>,
) -> HashMap<Pair, usize> {
    state
        .iter()
        .fold(HashMap::new(), |mut map, (&pair, count)| {
            let new_pairs = rules.get(&pair).unwrap();
            if *count > 0 {
                // one fewer of the originating pair
                // *map.entry(pair).or_default() += count;
                // one more of each of the new pairs
                *map.entry(new_pairs[0]).or_default() += count;
                *map.entry(new_pairs[1]).or_default() += count;
            }
            map
        })
}

impl DayFourteenSolver {
    fn run_polymerization(&self, rounds: usize) -> usize {
        // follow rules * 10
        let mut chain = self.start_state.clone();
        for _ in 0..rounds {
            chain = polymerize(&chain, &self.rules);
        }

        // find the most and least
        let mut char_count = HashMap::new();
        char_count.insert(&self.last_char, 1);
        let (min_count, max_count) = chain
            .iter()
            .fold(
                char_count,
                |mut map: HashMap<&char, usize>, (pair, count)| {
                    *map.entry(&pair.0).or_default() += count;
                    map
                },
            )
            .iter()
            .fold((usize::MAX, 0), |(min_count, max_count), (_ch, &count)| {
                (min_count.min(count), max_count.max(count))
            });
        max_count - min_count
    }
}

impl Solver for DayFourteenSolver {
    fn from_input(input: &str) -> Result<Box<Self>, String>
    where
        Self: Sized,
    {
        lazy_static! {
            static ref RULE_RE: Regex = Regex::new(r"([A-Z])([A-Z]) -> ([A-Z])").unwrap();
        }
        let mut line_iter = input.lines();

        let start_string = line_iter.next().unwrap();
        let start_state: HashMap<(char, char), usize> = start_string
            .chars()
            .zip(start_string.chars().skip(1))
            .fold(HashMap::new(), |mut map, pair| {
                *map.entry(pair).or_default() += 1;
                map
            });

        // skip the blank line
        line_iter.next();

        let mut rules: HashMap<Pair, [Pair; 2]> = HashMap::new();
        for line in line_iter {
            let captures = RULE_RE.captures(line).unwrap();
            let ch_1 = captures[1].chars().next().unwrap();
            let ch_2 = captures[2].chars().next().unwrap();
            let ch_lhs = captures[3].chars().next().unwrap();
            rules.insert((ch_1, ch_2), [(ch_1, ch_lhs), (ch_lhs, ch_2)]);
        }

        Ok(Box::new(DayFourteenSolver {
            rules,
            start_state,
            last_char: start_string.chars().last().unwrap(),
        }))
    }

    fn part_one(&self) -> Result<usize, &str> {
        Ok(self.run_polymerization(10))
    }

    fn part_two(&self) -> Result<usize, &str> {
        Ok(self.run_polymerization(40))
    }
}
