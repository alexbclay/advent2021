use std::{
    fmt::Display,
    ops::{Add, AddAssign},
};

use regex::Regex;

#[derive(Debug, PartialEq, Eq, Hash, Clone, Copy)]
pub struct Coord {
    pub x: i16,
    pub y: i16,
}

impl Coord {
    pub fn new(x: i16, y: i16) -> Coord {
        Coord { x, y }
    }

    pub fn draw_coord_window<'a, I>(from: Coord, to: Coord, coords: I)
    where
        I: Iterator<Item = &'a Coord>,
    {
        let min_x = from.x.min(to.x);
        let max_x = from.x.max(to.x);
        let len_x = (max_x - min_x) + 1;
        let min_y = from.y.min(to.y);
        let max_y = from.y.max(to.y);
        let len_y = (max_y - min_y) + 1;

        let mut window = vec![vec!['.'; len_x as usize]; len_y as usize];

        for coord in coords {
            if coord.x < min_x || coord.x > max_x || coord.y < min_y || coord.y > max_y {
                continue;
            }
            window[(coord.y - min_y) as usize][(coord.x - min_x) as usize] = '#';
        }

        for row in window.iter() {
            for val in row.iter() {
                print!("{}", val)
            }
            println!();
        }
    }

    pub fn get_all_neighbors(&self) -> [Coord; 8] {
        let coords_vec = [
            (-1, 0),
            (1, 0),
            (0, -1),
            (0, 1),
            (-1, 1),
            (-1, -1),
            (1, -1),
            (1, 1),
        ]
        .into_iter()
        .map(|(dx, dy)| Coord {
            x: self.x + dx,
            y: self.y + dy,
        })
        .collect::<Vec<Coord>>();
        [
            coords_vec[0],
            coords_vec[1],
            coords_vec[2],
            coords_vec[3],
            coords_vec[4],
            coords_vec[5],
            coords_vec[6],
            coords_vec[7],
        ]
    }
    pub fn get_orthogonal_neighbors(&self) -> [Coord; 4] {
        let coords_vec = [(-1, 0), (1, 0), (0, -1), (0, 1)]
            .into_iter()
            .map(|(dx, dy)| Coord {
                x: self.x + dx,
                y: self.y + dy,
            })
            .collect::<Vec<Coord>>();
        [coords_vec[0], coords_vec[1], coords_vec[2], coords_vec[3]]
    }
}

impl Add for Coord {
    type Output = Coord;

    fn add(self, rhs: Self) -> Self::Output {
        Coord {
            x: self.x + rhs.x,
            y: self.y + rhs.y,
        }
    }
}

impl AddAssign for Coord {
    fn add_assign(&mut self, rhs: Self) {
        self.x += rhs.x;
        self.y += rhs.y;
    }
}

impl Display for Coord {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "Coord({}, {})", self.x, self.y)
    }
}

impl TryFrom<&str> for Coord {
    type Error = String;

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        lazy_static! {
            static ref COORD_RE: Regex = Regex::new(r"(\d+),(\d+)").unwrap();
        }
        if let Some(captures) = COORD_RE.captures(value) {
            let x: Result<i16, _> = captures[1].parse();
            let y: Result<i16, _> = captures[2].parse();
            if let (Ok(x), Ok(y)) = (x, y) {
                Ok(Coord { x, y })
            } else {
                Err(format!("Could not parse x,y from {}", value))
            }
        } else {
            Err(format!(
                "Could not capture coordinates from string: {}",
                value
            ))
        }
    }
}
