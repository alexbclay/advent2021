use std::{collections::HashMap, fmt::Debug};

use crate::Solver;

pub struct DayFourSolver {
    nums: Vec<usize>,
    boards: Vec<BingoBoard>,
}

#[derive(Clone, Debug)]
enum BingoNum {
    Num(usize),
    Taken(usize),
}

#[derive(Clone)]
struct BingoBoard {
    id: usize,
    // full board
    numbers: Vec<Vec<BingoNum>>,
    // map of number to (row, col)
    by_numbers: HashMap<usize, (u8, u8)>,
    // map of row to count of called numbers
    by_row: [usize; 5],
    // map of col to count of called numbers
    by_col: [usize; 5],
}
impl BingoBoard {
    fn new(id: usize, numbers: Vec<Vec<BingoNum>>) -> BingoBoard {
        let mut by_numbers = HashMap::new();
        for (r, row) in numbers.iter().enumerate() {
            for (c, val) in row.iter().enumerate() {
                by_numbers.insert(
                    match val {
                        BingoNum::Num(n) => *n,
                        BingoNum::Taken(n) => *n,
                    },
                    (r as u8, c as u8),
                );
            }
        }
        BingoBoard {
            id,
            numbers,
            by_numbers,
            by_row: [0; 5],
            by_col: [0; 5],
        }
    }

    fn update_number(&mut self, num: &usize) -> bool {
        if let Some((row, col)) = self.by_numbers.get(num) {
            // this number exists on the board
            self.by_row[*row as usize] += 1;
            self.by_col[*col as usize] += 1;
            if let BingoNum::Num(n) = self.numbers[*row as usize][*col as usize] {
                self.numbers[*row as usize][*col as usize] = BingoNum::Taken(n);
            }

            // return true if either row or column has all five numbers correct
            return self.by_row[*row as usize] == 5 || self.by_col[*col as usize] == 5;
        }
        false
    }

    fn unmarked_number_sum(&self) -> usize {
        self.numbers
            .iter()
            .flatten()
            .filter_map(|x| {
                if let BingoNum::Num(n) = x {
                    Some(n)
                } else {
                    None
                }
            })
            .sum()
    }
}

impl Debug for BingoBoard {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        writeln!(f, "BingoBoard")?;
        for row in self.numbers.iter() {
            for val in row.iter() {
                match val {
                    BingoNum::Num(n) => write!(f, "{:>2} ", n),
                    BingoNum::Taken(_) => write!(f, " X "),
                }?;
            }
            writeln!(f)?;
        }
        f.debug_struct("BingoBoard")
            .field("by_row", &self.by_row)
            .field("by_col", &self.by_col)
            .finish()?;
        writeln!(f)
    }
}

impl Solver for DayFourSolver {
    fn from_input(input: &str) -> Result<Box<Self>, String>
    where
        Self: Sized,
    {
        let mut lines = input.lines();
        // collect the first line as the bingo numbers
        let nums = lines
            .next()
            .unwrap()
            .split(',')
            .map(|num| num.parse::<usize>().unwrap())
            .collect::<Vec<_>>();

        // Collect bingo boards one line at a time
        let mut board_id: usize = 1;
        let mut boards: Vec<BingoBoard> = Vec::new();
        let mut cur_board: Vec<Vec<BingoNum>> = Vec::new();
        for line in lines {
            if line.is_empty() {
                if !cur_board.is_empty() {
                    // the current board has data (ie not the first line)
                    // turn the rows into a BingoBoard
                    boards.push(BingoBoard::new(board_id, cur_board));
                    board_id += 1;
                }
                cur_board = Vec::new();
                continue;
            }
            let row = line
                .split_ascii_whitespace()
                .map(|num| BingoNum::Num(num.parse::<usize>().unwrap()))
                .collect();

            println!("{} -> {:?}", line, row);
            cur_board.push(row);
            println!("{:?}", cur_board);
        }
        // put any remaining info into a board when we've reached the end of the file
        boards.push(BingoBoard::new(board_id, cur_board));

        println!("{:?}", boards);
        Ok(Box::new(DayFourSolver { nums, boards }))
    }

    fn part_one(&self) -> Result<usize, &str> {
        let mut boards = self.boards.clone();
        for num in self.nums.iter() {
            println!("==== {} ====", num);
            for board in boards.iter_mut() {
                let win = board.update_number(num);

                if win {
                    return Ok(*num * board.unmarked_number_sum());
                }
            }
        }
        Err("No winner!")
    }

    fn part_two(&self) -> Result<usize, &str> {
        let mut boards = self.boards.clone();
        let mut last_board_score: usize = 0;
        let mut won_board_ids: Vec<usize> = vec![];
        for num in self.nums.iter() {
            println!("==== {} ====", num);
            for board in boards.iter_mut() {
                if won_board_ids.contains(&board.id) {
                    // no need to check for already winning boards
                    continue;
                }
                let win = board.update_number(num);
                if win {
                    won_board_ids.push(board.id);
                    println!("{:?}", board);
                    last_board_score = *num * board.unmarked_number_sum();
                }
            }
        }
        Ok(last_board_score)
    }
}
