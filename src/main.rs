use advent2021::{self, Solver};
use std::{error::Error, process};
use structopt::StructOpt;

#[derive(Debug, StructOpt)]
struct Opt {
    #[structopt(short = "d", long = "day", default_value = "1")]
    day: String,
    #[structopt(short = "i", long = "input", parse(from_os_str))]
    input: std::path::PathBuf,
}

fn main() -> Result<(), Box<dyn Error>> {
    let args = Opt::from_args();

    // load the file
    let content = std::fs::read_to_string(&args.input)?;

    let day: Box<dyn Solver> = match &args.day[..] {
        "1" => advent2021::day_1::DayOneSolver::from_input(&content)?,
        "2" => advent2021::day_2::DayTwoSolver::from_input(&content)?,
        "3" => advent2021::day_3::DayThreeSolver::from_input(&content)?,
        "4" => advent2021::day_4::DayFourSolver::from_input(&content)?,
        "5" => advent2021::day_5::DayFiveSolver::from_input(&content)?,
        "6" => advent2021::day_6::DaySixSolver::from_input(&content)?,
        "7" => advent2021::day_7::DaySevenSolver::from_input(&content)?,
        "8" => advent2021::day_8::DayEightSolver::from_input(&content)?,
        "9" => advent2021::day_9::DayNineSolver::from_input(&content)?,
        "10" => advent2021::day_10::DayTenSolver::from_input(&content)?,
        "11" => advent2021::day_11::DayElevenSolver::from_input(&content)?,
        "12" => advent2021::day_12::DayTwelveSolver::from_input(&content)?,
        "12_f" => advent2021::day_12_faster::DayTwelveSolver::from_input(&content)?,
        "13" => advent2021::day_13::DayThirteenSolver::from_input(&content)?,
        "14" => advent2021::day_14::DayFourteenSolver::from_input(&content)?,
        "15" => advent2021::day_15::DayFifteenSolver::from_input(&content)?,
        "16" => advent2021::day_16::DaySixteenSolver::from_input(&content)?,
        "17" => advent2021::day_17::DaySeventeenSolver::from_input(&content)?,
        "18" => advent2021::day_18::DayEighteenSolver::from_input(&content)?,
        // "25" => advent2021::day_twentyfive::DayTwentyFiveSolver::from_input(&content)?,
        _ => {
            eprintln!("Day {} is not implemented yet", &args.day);
            process::exit(1);
        }
    };

    println!("Part 1: {}", day.part_one()?);
    println!("Part 2: {}", day.part_two()?);

    Ok(())
}
